<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tdamblog' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Q.evXY:vds*}x0R$yHb(hbQ$%!g~uoBrM#EE7.x;y.[t{|NP9&XvP7!kpN =-I]O' );
define( 'SECURE_AUTH_KEY',  '$??$;|]BNRz6<XImm?Ka^G($d/tg*2*%E{S^CR=$1Hyf68&a@nhk.gxuM3 <3J:5' );
define( 'LOGGED_IN_KEY',    'lT@836pkyVhU+Hh(bpNo^2+tHA^Y,640rcvQj=nktr=MS*|1e^]?mzF;fnSM)#]T' );
define( 'NONCE_KEY',        '`{(lO$1R?,tIec;Ft4FT04op.?J0|-P2J)uJ>+Xe3~VF[R](f*&-Pmo/WD:gZS}7' );
define( 'AUTH_SALT',        ',}eqLzNOGIJ,hGI#7<BfH$`o<NtCqKJ_F}`e^ W[38i -cyJUW=M4Lq|]Aly^tcZ' );
define( 'SECURE_AUTH_SALT', 'q_s_,=Z[p*&u7xQmx8[$fe6_`v~b]E{s7*VC(T$ZGdqE*7q1),;FgunrqZ,HAr}h' );
define( 'LOGGED_IN_SALT',   '3Kd,r4lKa<A+%oJ,{4j#T`yZ!o4ZJDiZI`9IB^(>e+=iY?f],x#A@zRwdLfqw=!u' );
define( 'NONCE_SALT',       'fx;Gxkm]E1cF&7rq=SNi2r )U7:J#O%ST<qk$9{w@3u&FscHK[YtMP^i]iwp%bKH' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
